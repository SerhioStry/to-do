import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {AddTodoModalComponent} from '@app/todo/components/add-todo-modal/add-todo-modal.component';
import {ITodo} from '@app/core/interfaces/todo.interface';
import {TodoService} from '@app/todo/services/todo.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
    selector: 'app-todo-list',
    templateUrl: './todo-list.component.html',
    styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit, OnDestroy {

    @Input() todoList: ITodo[];

    todoListForDisplay: ITodo[];
    destroySubject$: Subject<void> = new Subject();

    constructor(public dialog: MatDialog, private todoService: TodoService) {
    }

    ngOnInit() {
        this.todoListForDisplay = this.todoList;
    }

    openDialog(): void {
        const dialogRef = this.dialog.open(AddTodoModalComponent, {
            width: '310px',
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.todoService.addTodo(result)
                    .pipe(takeUntil(this.destroySubject$))
                    .subscribe(todo => {
                        this.todoList = [...this.todoList, todo];
                        this.todoListForDisplay =  this.todoList;
                    });
            }
        });
    }

    ngOnDestroy() {
        this.destroySubject$.next();
    }

    // This part should be done as a pipe. Just becouse of a lack of time.

    searchTodoBeKeyWord(event: string): void {
        this.todoListForDisplay = [];

        if (event.length) {
            this.todoListForDisplay = this.todoList.filter(item => {
                if (item) {
                    return item.name.toLowerCase().includes(event.toLowerCase())
                        || item.description.toLowerCase().includes(event.toLowerCase());
                } else {
                    return false;
                }
            });
        } else {
            this.todoListForDisplay = this.todoList;
        }
    }

    searchForCompleted(event: boolean): void {
        this.todoListForDisplay = [];
        if (event) {
            this.todoListForDisplay = this.todoList.filter(item => item.isCompleted);
        } else {
            this.todoListForDisplay = this.todoList;
        }
    }

    deleteTodo(event) {
        this.todoListForDisplay = this.todoListForDisplay.filter(item => item.id !== event);
        this.todoList = this.todoListForDisplay;
    }

    editTodoInArray(event) {
       this.todoListForDisplay[this.todoListForDisplay.indexOf(this.todoListForDisplay.find(todo => todo.id === event.id))] = event;
    }
}
