import {Component, Input, OnDestroy, OnInit, Output, EventEmitter} from '@angular/core';
import {ITodo} from '@app/core/interfaces/todo.interface';
import {MatCheckboxChange} from '@angular/material';
import {TodoService} from '@app/todo/services/todo.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {AddTodoModalComponent} from '@app/todo/components/add-todo-modal/add-todo-modal.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
    selector: 'app-todo-item',
    templateUrl: './todo-item.component.html',
    styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent implements OnInit, OnDestroy {
    destroySubject$: Subject<void> = new Subject();
    @Input() todo: ITodo;
    @Output() deleteTodoEvent = new EventEmitter<number>()
    @Output() editTodoEvent = new EventEmitter<ITodo>()

    constructor(private todoService: TodoService, public dialog: MatDialog) {
    }

    ngOnInit() {
    }

    marckAsCompleted(event: MatCheckboxChange) {
        if (event.checked) {
            this.todo.isCompleted = true;
            this.todoService.editTodo(this.todo.id, this.todo)
                .pipe(takeUntil(this.destroySubject$))
                .subscribe(todo => this.todo = todo);
        } else {
            this.todo.isCompleted = false;
            this.todoService.editTodo(this.todo.id, this.todo)
                .pipe(takeUntil(this.destroySubject$))
                .subscribe(todo => this.todo = todo);
        }
    }

    deleteTodo() {
      this.todoService.deleteTodo(this.todo.id)
          .pipe(takeUntil(this.destroySubject$))
          .subscribe(() => {
              this.deleteTodoEvent.emit(this.todo.id);
          });
    }

    ngOnDestroy() {
        this.destroySubject$.next();
    }

    openDialog(): void {
        const dialogRef = this.dialog.open(AddTodoModalComponent, {
            width: '310px',
            data: {...this.todo}
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.todoService.editTodo(this.todo.id, result)
                    .pipe(takeUntil(this.destroySubject$))
                    .subscribe(todo => {
                        this.todo = todo;
                        this.editTodoEvent.emit(todo);
                    });
            }
        });
    }

}
