import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '@app/auth/services/auth.service';
import { Router } from '@angular/router';
import { IUser } from '@auth/models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
      if (authService.isAuthenticated()) {
          router.navigate(['/todo']);
      }
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      login: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required])
    });
}

  onSubmit() {
    const user: IUser = this.loginForm.value;
    this.authService.login(user);
  }
}
