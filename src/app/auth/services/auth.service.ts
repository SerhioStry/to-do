import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { IAuth } from '@auth/models/auth.model';
import { IUser } from '@auth/models/user.model';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class AuthService {
    readonly localStorageItemName = 'todo_token';
    constructor(
        private router: Router,
        private http: HttpClient,
    ) {}

    login(userData: IUser) {
        return this.http.post<IAuth>('http://localhost:3000/auth/login', userData)
            .subscribe(response => {
                this.saveToken(response);

                if (response.access_token) {
                    this.router.navigate(['/todo']);
                }
            });
    }

    private saveToken(authData: IAuth): void {
        localStorage.setItem(this.localStorageItemName, authData.access_token);
    }

    isAuthenticated(): boolean {
        const token = localStorage.getItem(this.localStorageItemName);
        return !!token;
    }

    getToken(): string {
        return localStorage.getItem(this.localStorageItemName);
    }


    logout(): void {
        localStorage.removeItem(this.localStorageItemName);
        this.router.navigate(['/login']);
    }



}
